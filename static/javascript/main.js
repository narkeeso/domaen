var Dm = klass({
  initialize: function() {
    var self = this;

    this.beforeReady();

    $(document).ready(function() {
      self.ready();
    });

    $(window).load(function() {
      self.render();
    });

    if (!$().velocity) {
      $('body').addClass('no-velocity-js');
    }
  },

  beforeReady: function() {},
  ready: function() {},
  render: function() {}
}).statics({
  create: function() {
    return new Dm();
  }
});

Dm.Main = Dm.extend({
  ready: function() {
    new Dm.Modals();
  },

  render: function() {
    new Dm.Packery();
  }
});

Dm.Packery = Dm.extend({
  initialize: function() {
    var options = this.constructor.options,
        target = this.constructor.target;

    $(target).imagesLoaded(function() {
      $(options.itemSelector).css('opacity', 1);
      $(target).packery(options);
    });
  }
}).statics({
  target: '.packery',

  options: {
    columnWidth: '.packery__thumb',
    itemSelector: '.packery__thumb',
    gutter: '.packery__gutter-sizer',
    stamp: '.packery__stamp'
  }
});

Dm.Modals = Dm.extend({
  options: {
    modal: {
      modalSelector: '.modal',
      modalTargetSelector: '.modal-target'
    },
    slicknav: {
      target: '#menu'
    }
  },

  initialize: function() {
    var options = this.options;

    this.setupMobileMenu(options.slicknav);
    this.setupModalHandler(options.modal);
  },

  setupMobileMenu: function(options) {
    $(function() {
      $(options.target).slicknav();
    });
  },

  setupModalHandler: function(options) {
    $(options.modalTargetSelector).on('click', function(event) {
      event.preventDefault();

      var $this = $(this),
          $body = $('body'),
          $content = $('.site-content'),
          targetName = $this.data('target');

      $('html').addClass('loading').spin();

      $body.on('shown.bs.modal', function() {
        $('html').removeClass('loading').spin(false);
        $content.css({
          height: '100%',
          overflow: 'hidden'
        });
      });

      $body.on('hide.bs.modal', function() {
        $content.css({
          height: 'auto',
          overflow: 'visible'
        });
      });

      $body.on('hidden.bs.modal', function() {
        $('.modal-content').remove();
      });

      $.get($this.data('modal'), function(data) {
        var $projectData = $(data).find('.modal-content')
          .attr('id', '#' + targetName);
        var $modal = $('body > .modal');

        $projectData.appendTo($modal.find('.modal-dialog'));
        $modal.modal('toggle');
      });
    });
  }
});

Dm.create();

// Main app entry point
new Dm.Main();
